package com.bitkiller.ProxyClassTry;

import junit.framework.TestCase;

import com.bitkiller.nullsafety.NullSafety;

/**
 * Unit test for simple App.
 */
public class NullSafetyTest extends TestCase {
	
	public void testSomething() {
		ClassA classA = new ClassA();

		System.out.println("NP: " + NullSafety.of(classA).getNotNullString());
		NullSafety.of(classA).sayHello();
		System.out.println("NP-B: "
				+ NullSafety.of(classA).getB().getNotNullString());

		ClassA classA2 = new ClassA();
		classA2.setB(new ClassB());
		System.out.println("NP-B: "
				+ NullSafety.of(classA2).getB().getNotNullString());

		ClassA realClassA = NullSafety.unbox(NullSafety.of(classA2));
		System.out.println("int " + NullSafety.of(classA).getB().getIntValue());
		System.out.println(NullSafety.of(classA) instanceof ClassA);

		System.out.println("int " + NullSafety.of(classA).getB().getContent());
		
		//System.out.println("A->B->MyEnum->nullB: " + NullSafety.of(classA).getB().getMyEnum().nullB());

	}
}
