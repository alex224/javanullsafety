package com.bitkiller.ProxyClassTry;

public class ClassB {
	private String content = null;
	
	private int intValue = 7;
	
	private TestEnum myEnum = null;
	
	public String getNotNullString() {
		return "b-notnull";
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getIntValue() {
		return intValue;
	}

	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}

	public TestEnum getMyEnum() {
		return myEnum;
	}

	public void setMyEnum(TestEnum myEnum) {
		this.myEnum = myEnum;
	}
}
