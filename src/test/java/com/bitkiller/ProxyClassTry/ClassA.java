package com.bitkiller.ProxyClassTry;

public class ClassA {

	
	private ClassB b = null;
	
	public ClassA() {
		
	}
	
	public void sayHello() {
		System.out.println("A says hello.");
	}
	public String getNotNullString() {
		return "notnull";
	}


	public ClassB getB() {
		return b;
	}


	public void setB(ClassB b) {
		this.b = b;
	}
}
