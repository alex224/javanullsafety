/**
 * 
 */
package com.bitkiller.nullsafety;

/**
 * @author alex
 * 
 */
public interface NullProxyInterface {
	Object getProxiedInstance();
}
